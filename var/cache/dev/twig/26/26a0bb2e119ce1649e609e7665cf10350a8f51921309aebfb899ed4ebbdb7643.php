<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* components/formVoterSond.html.twig */
class __TwigTemplate_8a6e8f6e732ce90838d30858a55593d9b40a9a7701c5ac58006aca6e0b4aa697 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "components/formVoterSond.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "components/formVoterSond.html.twig"));

        // line 1
        echo "<!-- VOTER SONDAGE FORM -->

<div class=\"container-fluid\">
    <div class=\"wrapper-form\">
        <form method=\"post\" class=\"p-4 mt-5\">

            <h2>L'avis de la jungle !</h2>

            ";
        // line 9
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, (isset($context["sondage"]) || array_key_exists("sondage", $context) ? $context["sondage"] : (function () { throw new RuntimeError('Variable "sondage" does not exist.', 9, $this->source); })()), "type", [], "any", false, false, false, 9), false))) {
            // line 10
            echo "                <p class=\"mb-5\">Une seul réponse</p>
            ";
        } else {
            // line 12
            echo "                <p class=\"mb-5\">Plusieurs réponses</p>
            ";
        }
        // line 14
        echo "
            ";
        // line 15
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["myForm"]) || array_key_exists("myForm", $context) ? $context["myForm"] : (function () { throw new RuntimeError('Variable "myForm" does not exist.', 15, $this->source); })()), 'form_start');
        echo "

                <p id=\"question\">";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["sondage"]) || array_key_exists("sondage", $context) ? $context["sondage"] : (function () { throw new RuntimeError('Variable "sondage" does not exist.', 17, $this->source); })()), "question", [], "any", false, false, false, 17), "html", null, true);
        echo "</p>

                <p>";
        // line 19
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["myForm"]) || array_key_exists("myForm", $context) ? $context["myForm"] : (function () { throw new RuntimeError('Variable "myForm" does not exist.', 19, $this->source); })()), "reponse", [], "any", false, false, false, 19), 'widget');
        echo "</p>

                <button class=\"btn mt-4\" type=\"submit\">Voter !</button>

            ";
        // line 23
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["myForm"]) || array_key_exists("myForm", $context) ? $context["myForm"] : (function () { throw new RuntimeError('Variable "myForm" does not exist.', 23, $this->source); })()), 'form_end');
        echo "

        </form>
    </div>
</div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "components/formVoterSond.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 23,  76 => 19,  71 => 17,  66 => 15,  63 => 14,  59 => 12,  55 => 10,  53 => 9,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!-- VOTER SONDAGE FORM -->

<div class=\"container-fluid\">
    <div class=\"wrapper-form\">
        <form method=\"post\" class=\"p-4 mt-5\">

            <h2>L'avis de la jungle !</h2>

            {% if(sondage.type == false) %}
                <p class=\"mb-5\">Une seul réponse</p>
            {% else %}
                <p class=\"mb-5\">Plusieurs réponses</p>
            {% endif %}

            {{ form_start(myForm) }}

                <p id=\"question\">{{ sondage.question }}</p>

                <p>{{ form_widget(myForm.reponse) }}</p>

                <button class=\"btn mt-4\" type=\"submit\">Voter !</button>

            {{ form_end(myForm) }}

        </form>
    </div>
</div>", "components/formVoterSond.html.twig", "/Users/kevinwolff/dev/growl-poll/templates/components/formVoterSond.html.twig");
    }
}
