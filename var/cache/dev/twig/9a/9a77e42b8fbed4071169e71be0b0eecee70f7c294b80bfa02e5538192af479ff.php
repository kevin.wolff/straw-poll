<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* components/formCreerSond.html.twig */
class __TwigTemplate_fac2659b862b91b0a70751ff8514abfef36969838f7bfe615dac0f0385fc4ec9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "components/formCreerSond.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "components/formCreerSond.html.twig"));

        // line 1
        echo "<!-- CREER SONDAGE FORM -->

<div class=\"container-fluid\">
    <div class=\"wrapper-form\">
        <form method=\"post\" class=\"p-4 mt-5\">

            <h2 class=\"mb-4\">Confrontez-vous à la jungle !</h2>

            ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["myForm"]) || array_key_exists("myForm", $context) ? $context["myForm"] : (function () { throw new RuntimeError('Variable "myForm" does not exist.', 9, $this->source); })()), 'form_start');
        echo "

                ";
        // line 11
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["myForm"]) || array_key_exists("myForm", $context) ? $context["myForm"] : (function () { throw new RuntimeError('Variable "myForm" does not exist.', 11, $this->source); })()), 'errors');
        echo "

                ";
        // line 13
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["myForm"]) || array_key_exists("myForm", $context) ? $context["myForm"] : (function () { throw new RuntimeError('Variable "myForm" does not exist.', 13, $this->source); })()), "question", [], "any", false, false, false, 13), 'row');
        echo "

                ";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["myForm"]) || array_key_exists("myForm", $context) ? $context["myForm"] : (function () { throw new RuntimeError('Variable "myForm" does not exist.', 15, $this->source); })()), "type", [], "any", false, false, false, 15), 'row');
        echo "

                ";
        // line 17
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["myForm"]) || array_key_exists("myForm", $context) ? $context["myForm"] : (function () { throw new RuntimeError('Variable "myForm" does not exist.', 17, $this->source); })()), "reponses", [], "any", false, false, false, 17), 'widget');
        echo "

                <button type=\"button\" class=\"btn add-reponse-sondage mb-4\">Ajouter réponses</button>

                <div class=\"reponses\">
                    <!-- Necessaire pour ajout de réponses -->
                </div>

                <button class=\"btn mt-4 type=submit\">Créer</button>

            ";
        // line 27
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["myForm"]) || array_key_exists("myForm", $context) ? $context["myForm"] : (function () { throw new RuntimeError('Variable "myForm" does not exist.', 27, $this->source); })()), 'form_end');
        echo "

        </form>
    </div>
</div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "components/formCreerSond.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 27,  73 => 17,  68 => 15,  63 => 13,  58 => 11,  53 => 9,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!-- CREER SONDAGE FORM -->

<div class=\"container-fluid\">
    <div class=\"wrapper-form\">
        <form method=\"post\" class=\"p-4 mt-5\">

            <h2 class=\"mb-4\">Confrontez-vous à la jungle !</h2>

            {{ form_start(myForm) }}

                {{ form_errors(myForm) }}

                {{ form_row(myForm.question) }}

                {{ form_row(myForm.type) }}

                {{ form_widget(myForm.reponses) }}

                <button type=\"button\" class=\"btn add-reponse-sondage mb-4\">Ajouter réponses</button>

                <div class=\"reponses\">
                    <!-- Necessaire pour ajout de réponses -->
                </div>

                <button class=\"btn mt-4 type=submit\">Créer</button>

            {{ form_end(myForm) }}

        </form>
    </div>
</div>", "components/formCreerSond.html.twig", "/Users/kevinwolff/dev/growl-poll/templates/components/formCreerSond.html.twig");
    }
}
