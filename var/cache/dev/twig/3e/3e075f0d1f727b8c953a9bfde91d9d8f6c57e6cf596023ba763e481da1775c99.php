<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* components/_header.html.twig */
class __TwigTemplate_000ef740dabffd1e15c45b4ec75d0e9d4588c5edc3387becc4e31f399ac1f969 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "components/_header.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "components/_header.html.twig"));

        // line 1
        echo "<!-- HEADER UNIVERSAL -->

<div class=\"header-desktop d-none d-sm-flex align-items-center\">
    <div class=\"container d-flex flex-column justify-content-center py-2\">

        <p>Vous voulez savoir comment un lion chasse ?</p>
        <p>N'allez pas zoo. Allez dans la jungle !</p>

        ";
        // line 9
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 9, $this->source); })()), "user", [], "any", false, false, false, 9)) {
            // line 10
            echo "            <a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("creer sondage");
            echo "\"> <button class=\"btn w-25 mx-auto my-3\">Créer votre sondage</button></a>
        ";
        } else {
            // line 12
            echo "            <a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_register");
            echo "\"> <button class=\"btn w-25 mx-auto my-3\">Inscrivez-vous</button></a>
        ";
        }
        // line 14
        echo "
    </div>
</div>

<div class=\"header-mobile d-flex d-sm-none align-items-center\">
    <div class=\"container d-flex flex-column align-items-center\">

        ";
        // line 21
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 21, $this->source); })()), "user", [], "any", false, false, false, 21)) {
            // line 22
            echo "            <a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("creer sondage");
            echo "\"> <button class=\"btn mx-auto my-3\">Créer votre sondage</button></a>
        ";
        } else {
            // line 24
            echo "            <a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_register");
            echo "\"> <button class=\"btn mx-auto my-3\">Inscrivez-vous</button></a>
        ";
        }
        // line 26
        echo "
    </div>
</div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "components/_header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 26,  84 => 24,  78 => 22,  76 => 21,  67 => 14,  61 => 12,  55 => 10,  53 => 9,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!-- HEADER UNIVERSAL -->

<div class=\"header-desktop d-none d-sm-flex align-items-center\">
    <div class=\"container d-flex flex-column justify-content-center py-2\">

        <p>Vous voulez savoir comment un lion chasse ?</p>
        <p>N'allez pas zoo. Allez dans la jungle !</p>

        {% if app.user %}
            <a href=\"{{ path('creer sondage') }}\"> <button class=\"btn w-25 mx-auto my-3\">Créer votre sondage</button></a>
        {% else  %}
            <a href=\"{{ path('app_register') }}\"> <button class=\"btn w-25 mx-auto my-3\">Inscrivez-vous</button></a>
        {% endif %}

    </div>
</div>

<div class=\"header-mobile d-flex d-sm-none align-items-center\">
    <div class=\"container d-flex flex-column align-items-center\">

        {% if app.user %}
            <a href=\"{{ path('creer sondage') }}\"> <button class=\"btn mx-auto my-3\">Créer votre sondage</button></a>
        {% else  %}
            <a href=\"{{ path('app_register') }}\"> <button class=\"btn mx-auto my-3\">Inscrivez-vous</button></a>
        {% endif %}

    </div>
</div>", "components/_header.html.twig", "/Users/kevinwolff/dev/growl-poll/templates/components/_header.html.twig");
    }
}
