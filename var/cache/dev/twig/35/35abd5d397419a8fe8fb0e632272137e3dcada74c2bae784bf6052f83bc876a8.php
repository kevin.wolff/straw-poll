<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* components/displayAllSond.html.twig */
class __TwigTemplate_59c2862d540ebaae903e345a1ff3ea95479fabf9324988a559e5fde7cbb77c31 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "components/displayAllSond.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "components/displayAllSond.html.twig"));

        // line 1
        echo "<!-- SONDAGES DISPLAY - HOME -->

<div class=\"sondageDisplay\">

    ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["sondages"]) || array_key_exists("sondages", $context) ? $context["sondages"] : (function () { throw new RuntimeError('Variable "sondages" does not exist.', 5, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["sondage"]) {
            // line 6
            echo "        <div class='sondage'>

            <h3>";
            // line 8
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["sondage"], "question", [], "any", false, false, false, 8), "html", null, true);
            echo "</h3>
            <p> par : ";
            // line 9
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["sondage"], "pseudo", [], "any", false, false, false, 9), "html", null, true);
            echo "</p>

            <div class=\"boutons mt-4\">

                ";
            // line 13
            if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 13, $this->source); })()), "user", [], "any", false, false, false, 13)) {
                // line 14
                echo "                    <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("voter sondage", ["id" => twig_get_attribute($this->env, $this->source, $context["sondage"], "id", [], "any", false, false, false, 14)]), "html", null, true);
                echo "\"><button class=\"btn\">Voter</button></a>
                ";
            }
            // line 16
            echo "                <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("consulter sondage", ["id" => twig_get_attribute($this->env, $this->source, $context["sondage"], "id", [], "any", false, false, false, 16)]), "html", null, true);
            echo "\"><button class=\"btn\">Resultats</button></a>

            </div>

        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sondage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "
</div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "components/displayAllSond.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 22,  76 => 16,  70 => 14,  68 => 13,  61 => 9,  57 => 8,  53 => 6,  49 => 5,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!-- SONDAGES DISPLAY - HOME -->

<div class=\"sondageDisplay\">

    {% for sondage in sondages %}
        <div class='sondage'>

            <h3>{{ sondage.question }}</h3>
            <p> par : {{ sondage.pseudo }}</p>

            <div class=\"boutons mt-4\">

                {% if app.user %}
                    <a href=\"{{path('voter sondage',{id: sondage.id})}}\"><button class=\"btn\">Voter</button></a>
                {% endif %}
                <a href=\"{{path('consulter sondage',{id: sondage.id})}}\"><button class=\"btn\">Resultats</button></a>

            </div>

        </div>
    {% endfor %}

</div>", "components/displayAllSond.html.twig", "/Users/kevinwolff/dev/straw-poll/templates/components/displayAllSond.html.twig");
    }
}
