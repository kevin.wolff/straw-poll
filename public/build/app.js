(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app"],{

/***/ "./assets/js/app.js":
/*!**************************!*\
  !*** ./assets/js/app.js ***!
  \**************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_find__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.find */ "./node_modules/core-js/modules/es.array.find.js");
/* harmony import */ var core_js_modules_es_array_find__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_find__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.regexp.exec */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.string.replace */ "./node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _sass_app_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../sass/app.scss */ "./assets/sass/app.scss");
/* harmony import */ var _sass_app_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_sass_app_scss__WEBPACK_IMPORTED_MODULE_3__);



// Custom import SCSS
 // Script pour formulaire sondage

jQuery(document).ready(function () {
  var $collectionHolder = $('#sondage_form_reponses');
  var $addQuestionButton = $('.add-reponse-sondage');
  $collectionHolder.data('index', $collectionHolder.find('input').length);
  $addQuestionButton.on('click', function (e) {
    addTagForm($collectionHolder, $addQuestionButton);
  });
});

function addTagForm($collectionHolder, $newLinkLi) {
  var prototype = $collectionHolder.data('prototype');
  var index = $collectionHolder.data('index');
  var newForm = prototype;
  newForm = newForm.replace(/__name__/g, index);
  $collectionHolder.data('index', index + 1);
  var $newFormLi = $('<p></p>').append(newForm);
  $newLinkLi.before($newFormLi);
} // TEST


console.log('Javascript opérationnel');

/***/ }),

/***/ "./assets/sass/app.scss":
/*!******************************!*\
  !*** ./assets/sass/app.scss ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

},[["./assets/js/app.js","runtime","vendors~app"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvYXBwLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9zYXNzL2FwcC5zY3NzIl0sIm5hbWVzIjpbImpRdWVyeSIsImRvY3VtZW50IiwicmVhZHkiLCIkY29sbGVjdGlvbkhvbGRlciIsIiQiLCIkYWRkUXVlc3Rpb25CdXR0b24iLCJkYXRhIiwiZmluZCIsImxlbmd0aCIsIm9uIiwiZSIsImFkZFRhZ0Zvcm0iLCIkbmV3TGlua0xpIiwicHJvdG90eXBlIiwiaW5kZXgiLCJuZXdGb3JtIiwicmVwbGFjZSIsIiRuZXdGb3JtTGkiLCJhcHBlbmQiLCJiZWZvcmUiLCJjb25zb2xlIiwibG9nIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7Q0FJQTs7QUFFQUEsTUFBTSxDQUFDQyxRQUFELENBQU4sQ0FBaUJDLEtBQWpCLENBQXVCLFlBQVc7QUFFOUIsTUFBSUMsaUJBQWlCLEdBQUdDLENBQUMsQ0FBQyx3QkFBRCxDQUF6QjtBQUNBLE1BQUlDLGtCQUFrQixHQUFHRCxDQUFDLENBQUMsc0JBQUQsQ0FBMUI7QUFDQUQsbUJBQWlCLENBQUNHLElBQWxCLENBQXVCLE9BQXZCLEVBQWdDSCxpQkFBaUIsQ0FBQ0ksSUFBbEIsQ0FBdUIsT0FBdkIsRUFBZ0NDLE1BQWhFO0FBRUFILG9CQUFrQixDQUFDSSxFQUFuQixDQUFzQixPQUF0QixFQUErQixVQUFTQyxDQUFULEVBQVk7QUFDdkNDLGNBQVUsQ0FBQ1IsaUJBQUQsRUFBb0JFLGtCQUFwQixDQUFWO0FBQ0gsR0FGRDtBQUdILENBVEQ7O0FBV0EsU0FBU00sVUFBVCxDQUFvQlIsaUJBQXBCLEVBQXVDUyxVQUF2QyxFQUFtRDtBQUMvQyxNQUFJQyxTQUFTLEdBQUdWLGlCQUFpQixDQUFDRyxJQUFsQixDQUF1QixXQUF2QixDQUFoQjtBQUVBLE1BQUlRLEtBQUssR0FBR1gsaUJBQWlCLENBQUNHLElBQWxCLENBQXVCLE9BQXZCLENBQVo7QUFDQSxNQUFJUyxPQUFPLEdBQUdGLFNBQWQ7QUFFQUUsU0FBTyxHQUFHQSxPQUFPLENBQUNDLE9BQVIsQ0FBZ0IsV0FBaEIsRUFBNkJGLEtBQTdCLENBQVY7QUFDQVgsbUJBQWlCLENBQUNHLElBQWxCLENBQXVCLE9BQXZCLEVBQWdDUSxLQUFLLEdBQUcsQ0FBeEM7QUFDQSxNQUFJRyxVQUFVLEdBQUdiLENBQUMsQ0FBQyxTQUFELENBQUQsQ0FBYWMsTUFBYixDQUFvQkgsT0FBcEIsQ0FBakI7QUFDQUgsWUFBVSxDQUFDTyxNQUFYLENBQWtCRixVQUFsQjtBQUNILEMsQ0FFRDs7O0FBRUFHLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLHlCQUFaLEU7Ozs7Ozs7Ozs7O0FDL0JBLHVDIiwiZmlsZSI6ImFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIEN1c3RvbSBpbXBvcnQgU0NTU1xuXG5pbXBvcnQgJy4uL3Nhc3MvYXBwLnNjc3MnO1xuXG4vLyBTY3JpcHQgcG91ciBmb3JtdWxhaXJlIHNvbmRhZ2VcblxualF1ZXJ5KGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcblxuICAgIHZhciAkY29sbGVjdGlvbkhvbGRlciA9ICQoJyNzb25kYWdlX2Zvcm1fcmVwb25zZXMnKTtcbiAgICB2YXIgJGFkZFF1ZXN0aW9uQnV0dG9uID0gJCgnLmFkZC1yZXBvbnNlLXNvbmRhZ2UnKTtcbiAgICAkY29sbGVjdGlvbkhvbGRlci5kYXRhKCdpbmRleCcsICRjb2xsZWN0aW9uSG9sZGVyLmZpbmQoJ2lucHV0JykubGVuZ3RoKTtcblxuICAgICRhZGRRdWVzdGlvbkJ1dHRvbi5vbignY2xpY2snLCBmdW5jdGlvbihlKSB7XG4gICAgICAgIGFkZFRhZ0Zvcm0oJGNvbGxlY3Rpb25Ib2xkZXIsICRhZGRRdWVzdGlvbkJ1dHRvbik7XG4gICAgfSk7XG59KTtcblxuZnVuY3Rpb24gYWRkVGFnRm9ybSgkY29sbGVjdGlvbkhvbGRlciwgJG5ld0xpbmtMaSkge1xuICAgIHZhciBwcm90b3R5cGUgPSAkY29sbGVjdGlvbkhvbGRlci5kYXRhKCdwcm90b3R5cGUnKTtcblxuICAgIHZhciBpbmRleCA9ICRjb2xsZWN0aW9uSG9sZGVyLmRhdGEoJ2luZGV4Jyk7XG4gICAgdmFyIG5ld0Zvcm0gPSBwcm90b3R5cGU7XG5cbiAgICBuZXdGb3JtID0gbmV3Rm9ybS5yZXBsYWNlKC9fX25hbWVfXy9nLCBpbmRleCk7XG4gICAgJGNvbGxlY3Rpb25Ib2xkZXIuZGF0YSgnaW5kZXgnLCBpbmRleCArIDEpO1xuICAgIHZhciAkbmV3Rm9ybUxpID0gJCgnPHA+PC9wPicpLmFwcGVuZChuZXdGb3JtKTtcbiAgICAkbmV3TGlua0xpLmJlZm9yZSgkbmV3Rm9ybUxpKTtcbn1cblxuLy8gVEVTVFxuXG5jb25zb2xlLmxvZygnSmF2YXNjcmlwdCBvcMOpcmF0aW9ubmVsJyk7IiwiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luIl0sInNvdXJjZVJvb3QiOiIifQ==