// Custom import SCSS

import '../sass/app.scss';

// Script pour formulaire sondage

jQuery(document).ready(function() {

    var $collectionHolder = $('#sondage_form_reponses');
    var $addQuestionButton = $('.add-reponse-sondage');
    $collectionHolder.data('index', $collectionHolder.find('input').length);

    $addQuestionButton.on('click', function(e) {
        addTagForm($collectionHolder, $addQuestionButton);
    });
});

function addTagForm($collectionHolder, $newLinkLi) {
    var prototype = $collectionHolder.data('prototype');

    var index = $collectionHolder.data('index');
    var newForm = prototype;

    newForm = newForm.replace(/__name__/g, index);
    $collectionHolder.data('index', index + 1);
    var $newFormLi = $('<p></p>').append(newForm);
    $newLinkLi.before($newFormLi);
}

// TEST

console.log('Javascript opérationnel');