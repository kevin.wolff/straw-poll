<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    ////// Home utilisateur //////

    /**
     * @Route("/user/home/{id}", name="accueil utilisateur")
     */
    public function accueilUser($id)
    {
        return $this->render('user/userHome.html.twig');
    }
}
