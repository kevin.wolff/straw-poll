<?php

namespace App\Controller;

use App\Entity\Reponse;
use App\Entity\Sondage;
use App\Entity\Vote;
use App\Form\SondageFormType;
use App\Form\VoteMultiFormType;
use App\Form\VoteFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class SondageController extends AbstractController
{

   ////// Home page display all sondages //////

    /**
     * @Route("/", name="home sondage")
     */
    public function homeSondage()
    {
        $sondages = $this->getDoctrine()
            ->getRepository(Sondage::class)
            ->findAll();

        return $this->render('sondage/home.html.twig', [
            'sondages' => $sondages
        ]);
    }

    ////// Créer un nouveau sondage //////

    /**
     * @Route("/sondage/creer", name="creer sondage")
     */
    public function newSondage(Request $request, EntityManagerInterface $entityManager)
    {
        // Nouveau sondage
        $sondage = new Sondage();
        $pseudo = $this->getUser()->getPseudo();
        $sondage->setPseudo($pseudo);
        $form = $this->createForm(SondageFormType::class, $sondage);
        $form->handleRequest($request);

        // Si le formulaire est valide l'ajoute à la bdd
        if($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($sondage);
            $entityManager->flush();
            return $this->redirectToRoute('home sondage');
        }

        return $this->render('sondage/creer.html.twig', [
            'myForm' => $form->createView()
        ]);
    }

    ////// Voter à un sondage //////

    /**
     * @Route("/sondage/voter/{id}", name="voter sondage")
     */
    public function voterSondage($id, Request $request, EntityManagerInterface $entityManager)
    {

        $sondage = $this->getDoctrine()
            ->getRepository(Sondage::class)
            ->find($id);

        $test = $sondage->getReponses();

        // Nouveau vote à un sondage
        $vote = new Vote();
        $form = $this->createForm(VoteFormType::class, $vote, [
            'userPseudo' => $this->getUser()->getPseudo(),
            'sondageType' => $sondage->getType(),
            'reponses' => $sondage->getReponses()
        ]);
        $form->handleRequest($request);

        // Si le formulaire est valide l'ajoute à la bdd
        if($form->isSubmitted() && $form->isValid())
        {
            $reponseType = is_array($form['reponse']->getData());

            if ($reponseType === true)
            {
                foreach($form['reponse']->getData() as $reponse) {
                    $vote->addReponse($reponse);
                }
            } else
            {
                $reponse = $form['reponse']->getData();
                $vote->addReponse($reponse);
            }

            $entityManager->persist($vote);
            $entityManager->flush();
            return $this->redirectToRoute('home sondage');
        }

        return $this->render('sondage/voter.html.twig', [
            'sondage' => $sondage, 'myForm' => $form->createView()
        ]);
    }

    ////// Consulter un sondage - WORK IN PROGRESS !!! //////

    /**
     * @Route("/sondage/consulter/{id}", name="consulter sondage")
     */
    public function consulterSondage($id, EntityManagerInterface $entityManager)
    {
        $sondage = $this->getDoctrine()
            ->getRepository(Sondage::class)
            ->find($id);

        $reponses = $this->getDoctrine()
            ->getRepository(Reponse::class)
            ->findBy([
                'sondage' => $id
            ]);

        foreach ($reponses as $reponse)
        {
            $test = $reponse->getId();
        }

        dump($test);exit();

        return $this->render('sondage/consulter.html.twig', [
            'sondage' => $sondage
        ]);
    }
}
