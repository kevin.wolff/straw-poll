<?php

namespace App\Form;

use App\Entity\Vote;
use Doctrine\DBAL\Types\IntegerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VoteFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // Pseudo - non visible
            ->add('userPseudo', HiddenType::class, [
                'data' => $options['userPseudo'],
            ])
            // ID de la reponse
            ->add('reponse', ChoiceType::class, [
                'choices' => $options['reponses'],
                'choice_value' => 'id',
                'choice_label' => 'reponse',
                'multiple' => $options['sondageType'],
                'expanded' => true,
                'mapped' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'userPseudo' => false,
            'sondageType' => false,
            'reponses' => []
        ]);
    }
}
